<?php

/**
 * @file
 * Core user actions with GitLab
 */

include_once 'gitlab_users.features.inc';

/**
 * Get all users
 *
 * @param string $private_token
 *  The user's private token for connecting to the api endpoint
 * @param array $options [optional]
 *  List of extra options
 * @return array
 *  Returns the list of gitlab users, FALSE otherwise.
 */
function gitlab_users_get_users($private_token, $options = array()) {
  module_load_include('inc', 'gitlab_users');
  // @todo add access control
  return _gitlab_users_users('index', NULL, $private_token, $options);

  return FALSE;
}

